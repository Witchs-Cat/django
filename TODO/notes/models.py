from django.db.models import Model, CASCADE, ForeignKey, TextField, IntegerField, CharField, DateTimeField
from django.utils.timezone import now
from typing import List

# Create your models here.

class Author(Model):
    
    id = IntegerField(
        primary_key = True, 
        null=False, 
        unique=True,
        blank=True
    )
    
    username = CharField(
        verbose_name = "Публичное имя пользователя",
        max_length = 100,
        unique=True,
    )

    registration_date = DateTimeField(
        verbose_name = "Дата регестрации",
        default = now,
        null = True,
        blank = True
    )

    @property
    def memos(self):
        return Memo.objects.filter(author__id = self.id)
        
    def __str__(self) -> str:
        return self.username

class MemoTag(Model):
    id = IntegerField(primary_key = True)

    name = CharField(
        max_length = 100
    )

    def __str__(self) -> str:
        return f"{self.id}:{self.name}"

class TagMemoRelationship(Model):
    memo = ForeignKey(
        to = "Memo",
        on_delete = CASCADE
    )

    tag = ForeignKey(
        to = "MemoTag",
        on_delete = CASCADE
    )

    def __str__(self) -> str:
        return f"{self.memo}={self.tag}"

class Memo(Model):
    id = IntegerField(primary_key = True)

    author = ForeignKey(
        to = "Author",
        on_delete = CASCADE
    )

    content = TextField(
        verbose_name = "Содержание"
    )

    time_create = DateTimeField(
        verbose_name = "Дата создания записи",
        default = now,
    )

    @property
    def tags(self) -> List[MemoTag]:
        relationships = TagMemoRelationship.objects.filter(memo__id = self.id)
        return [rs.tag for rs in relationships]

    def add_tag(self, tag: MemoTag) -> None:
        TagMemoRelationship.objects.create(memo = self, tag = tag)

    def remove_tag(self, tag: MemoTag) -> None:
        TagMemoRelationship.objects.filter(memo__id = self.id, tag__id = tag.id).delete()
    
    def __str__(self) -> str:
        return f"{self.id}:{self.author}:{self.time_create}"


