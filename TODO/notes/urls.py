from django.urls import path, include
from .views import AuthorView, MemoView, AuthorMemosView, AuthorViewSet, MemoTagViewSet, MemoViewSet, TagMemoRelationshipSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'authors', AuthorViewSet)
router.register(r'tags', MemoTagViewSet)
router.register(r'memos', MemoViewSet)
router.register(r'ships', TagMemoRelationshipSet)

urlpatterns = router.urls
urlpatterns += [
    path(r'author/<str:authorId>', AuthorView.as_view()),
    path(r'author/<str:authorId>/memos', AuthorMemosView.as_view()),
    path(r'memo/<int:memoId>', MemoView.as_view()),
    ]