from rest_framework.serializers import *
from .models import Author, MemoTag, TagMemoRelationship, Memo
from typing import List

class MemoTagSerializer(ModelSerializer):    
    class Meta:
        model = MemoTag
        fields = '__all__'

class AuthorSerializer(ModelSerializer):    
    #memos = SerializerMethodField(method_name='get_memos')
    
    class Meta:
        model = Author
        fields = '__all__'

    def get_memos(self, author: Author)->List:
        return [MemoSerializer(memo).data for memo in author.memos]

class MemoSerializer(ModelSerializer):    
    tags = SerializerMethodField(method_name='get_tags')

    class Meta:
        model = Memo
        fields = '__all__'

    def get_tags(self, memo: Memo)->List:
        return [MemoTagSerializer(tag).data for tag in memo.tags]
    

class TagMemoRelationshipSerializer(ModelSerializer):    
    class Meta:
        model = TagMemoRelationship
        fields = '__all__'