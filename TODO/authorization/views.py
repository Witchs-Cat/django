from telnetlib import STATUS
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.status import HTTP_400_BAD_REQUEST
# Create your views here.

class Registration(APIView):
    permission_classes = [AllowAny]
    #authentication_classes = [BasicAuthentication]

    def post(self, request, *args, **kwargs):
        date = {}
        badRequestData = {}
        for i in ['username', 'email', 'password']:
            date[i] = request.data.get(i, None)
            if date[i] is None:
                badRequestData[i] = "Обязателньное поле"
            
        if len(badRequestData):
            return Response(status = HTTP_400_BAD_REQUEST, data = badRequestData)

        if User.objects.filter(username = date['username']):
             return Response(status = HTTP_400_BAD_REQUEST, data = {'detail':'имя пользователя занято'})

        user = User.objects.create_user(**date)
        return Response({
            'token': Token.objects.get(user__id = user.id).key
            })