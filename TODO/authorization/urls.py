from posixpath import basename
from django.urls import path, include
from .views import Registration
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = [
    path('signup', Registration.as_view()),
    path('login', obtain_auth_token)
]